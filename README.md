# imguraspdf

### Download Imgur albums as PDF

#### Installation

`
    pip3 install -r requirements.txt
`

You may also have to install `ghostscript` for pdf compression

#### Example

`
    python3 imguraspdf.py lDRB2
`
