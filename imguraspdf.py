#!/usr/bin/python3
# -*- coding: utf-8 -*-
import argparse
import os
import os.path
from shutil import rmtree
import threading
import subprocess

import colorama
from colorama import Fore, Back, Style
from fake_useragent import UserAgent
import img2pdf
from PIL import Image
import requests
from tqdm import tqdm

CACHE_DIR = '.cache'
THREADS = 10

def download(chunk):
    for name, link in chunk:
        response = requests.get(link, stream=True)
        total = int(response.headers.get('content-length', 0))
        filename = f'./{CACHE_DIR}/{name}.png'
        with open(filename, 'wb+') as file, tqdm(
            desc=f'Image #{name}',
            total=total,
            unit='iB',
            unit_scale=True,
            unit_divisor=1024,
        ) as bar:
            for data in response.iter_content(chunk_size=1024):
                size = file.write(data)
                bar.update(size)


def slice_list(seq, num):
    avg = len(seq) / float(num)
    out = []
    last = 0.0
    while last < len(seq):
        out.append(seq[int(last):int(last + avg)])
        last += avg
    return out


def main(album_id, uncompressed):
    if album_id[-1] == '/':
        album_id = album_id[:-1]
    album_id = album_id.split('/')[-1]
    if not os.path.exists(CACHE_DIR):
        os.makedirs(CACHE_DIR)
    useragent = UserAgent()
    hdr = {
        'Authorization' : 'Client-ID 5dc6065411ee2ab', # 920c7e989ed4deb
        'User-Agent' : str(useragent.chrome)
    }
    response = requests.get(f'https://api.imgur.com/3/album/{album_id}', headers=hdr)
    rjson = response.json()
    if not rjson['success']:
        print(Style.BRIGHT + Fore.RED + f'Couldn\'t get album {album_id}!')
        print('Reason: ' + Fore.RED + rjson['data']['error'])
        return
    response_data = rjson['data']
    title = response_data['title']
    images = response_data['images']
    print(Fore.CYAN + f'Downloading {title}, {len(images)} images...')
    links = []
    for i in range(len(images)):
        links.append((str(i), images[i]['link']))
    total = len(links)
    links = slice_list(links, THREADS)
    threads = []
    for chunk in links:
        threads.append(threading.Thread(target=download, args=(chunk,)))
        threads[-1].start()
    for t in threads:
        t.join()
    filenames = []
    for i in range(total):
        filenames.append(CACHE_DIR + '/' + str(i) + '.png')
    final_name_uncompressed = f'{title}-uncompressed.pdf'
    try:
        with open(final_name_uncompressed, 'wb+') as file:
            file.write(img2pdf.convert(*filenames))
    except:
        for image in filenames:
            image_file = Image.open(image)
            if image_file.mode == "RGBA":
                image_file = image_file.convert("RGB")
            image_file.save(image, 'png')
            image_file.close()
        with open(final_name_uncompressed, 'wb+') as file:
            file.write(img2pdf.convert(*filenames))
    final_name = f'{title}.pdf'
    if uncompressed:
        os.rename(final_name_uncompressed, final_name)
    else:
        subprocess.call(['gs', '-sDEVICE=pdfwrite', '-dCompatibilityLevel=1.4',
                        '-dPDFSETTINGS={}'.format('/ebook'),
                        '-dNOPAUSE', '-dQUIET', '-dBATCH',
                        '-sOutputFile={}'.format(final_name),
                        final_name_uncompressed]
        )
        os.remove(final_name_uncompressed)
    rmtree(CACHE_DIR)


if __name__ == '__main__':
    colorama.init(autoreset=True)
    parser = argparse.ArgumentParser(description='album2pdf: download imgur albums as pdf')
    parser.add_argument(
        'album_id',
        action='store',
        help='Album id (imgur.com/a/xxxxxxx)')
    parser.add_argument(
        '--uncompressed',
        dest='uc',
        action='store_const',
        const=True,
        default=False,
        help='Do not compress pdf')
    args = parser.parse_args()
    main(args.album_id, args.uc)